// problem5.js

const problem5 = (carYears) => {
    if (!carYears || carYears.length === 0) {
      console.log("Array of car years is empty or undefined. Unable to find older cars.");
      return []; // Return an empty array or handle as appropriate
    }
    try {
      // Filter car years before 2000
      const olderCars = carYears.filter(year => year < 2000);
      // Log the length of olderCars array
      console.log(`Number of cars older than the year 2000: ${olderCars.length}`);
      // Return the array of older cars if needed for testing or further use
      return olderCars;
    } catch (error) {
      console.error("Error filtering older cars:", error.message);
      return []; // Return an empty array or handle the error as appropriate
    }
  };
  module.exports = problem5;
  