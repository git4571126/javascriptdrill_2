// test/testProblem5.js

const problem5 = require('../problem5'); // Adjust the path as necessary
const problem4 = require('../problem4'); // Assuming problem4 is implemented 
const inventory = require('./inventory');

// Call problem4 to get car years
const carYears = problem4(inventory);

// Call problem5 with carYears array
const olderCars = problem5(carYears);

// Additional testing or assertions as needed
