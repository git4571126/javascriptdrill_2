// problem1.js
const problem1 = (inventory) => {
    // Find the car with id 33
    const car = inventory.filter(car => car.id === 33)[0]; //
    
    // Extract car information
    const { car_year, car_make, car_model } = car;
  
    // Format output string
   
      console.log(`Car 33 is a ${car_year} ${car_make} ${car_model}`);
  };
  // Export the function for testing
  module.exports = problem1;
  