// problem6.js

const problem6 = (inventory) => {
    // Check if inventory is empty
    if (!inventory || inventory.length === 0) {
      console.log("Inventory is empty or undefined. Unable to filter BMW and Audi cars.");
      return []; // Return an empty array 
    }
  
    try {
      // Filter BMW and Audi cars
      const BMWAndAudi = inventory.filter(car => car.car_make === 'BMW' || car.car_make === 'Audi');
  
      // Converting BMWAndAudi array to JSON string
      const BMWAndAudiString = JSON.stringify(BMWAndAudi);
      console.log(BMWAndAudiString);
  
      // Return the array of BMW and Audi car
      return BMWAndAudi;
    } catch (error) {
      console.error("Error filtering BMW and Audi cars:", error.message);
      return []; // Return an empty array or handle the error as appropriate
    }
  }
  module.exports = problem6;
  