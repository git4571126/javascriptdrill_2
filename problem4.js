const problem4 = (inventory) => {
    // Check if inventory is empty 
    if (!inventory || inventory.length === 0) {
      console.log("Inventory is empty or undefined. Unable to retrieve car years.");
      return []; //return empty array in such case
    }

    try {
      // Extract caryears using map()
      const carYears = inventory.map(car => car.car_year);

      console.log(carYears);
  
      // Return the array of car years if needed for testing or further use
      return carYears;
    } catch (error) {
      console.error("Error extracting car years:", error.message);
      return []; // Return an empty array or handle the error as appropriate
    }
  };
  module.exports = problem4;